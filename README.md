# ManaLynx

<img src="https://i.imgur.com/aDHj38L.png" width="650" height="650" />

## Elementos

- **Product Owner** - Miguel Silva Nº18829
- **Scrum Master** &nbsp;&nbsp;- Leonel Fernandes Nº18850
- **Developer** - André Cardoso Nº18848
- **Developer** - José Cosgrove Nº18826
- **Developer** - Henrique Senra Nº21154


## Introdução
ManaLynx é uma plataforma que procura simplificar a vida dos agentes de seguros, isto através de uma plataforma que permite ao agente fazer todo o processo de emissão de apolice num único sítio! 
A plataforma WEB procura também facilitar aos clientes o pedido de simulações e gestão das mesmas desde a fase de ativação até a gestão de ocorrências/sinistros. 
Além do descrito também é possível efetuar a gestão das equipas de agentes através de um dashboard dinâmico onde encontra tudo o necessário para levar a sua equipa ao sucesso.   

## Dashboard


![](https://i.imgur.com/VghX3ny.png)

