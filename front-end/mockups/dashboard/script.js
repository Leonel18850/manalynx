function onLoad(){
    document.getElementById("agenteEdit").style.display = "none"
}

// Hover Status Navbar
let list = document.querySelectorAll(".navigation ul li");

list.forEach((item)=>item.addEventListener("click", activeLink));

// FAQ

let question = document.querySelectorAll(".question");

question.forEach(question => {
  question.addEventListener("click", event => {
    const active = document.querySelector(".question.active");
    if(active && active !== question ) {
      active.classList.toggle("active");
      active.nextElementSibling.style.maxHeight = 0;
    }
    question.classList.toggle("active");
    const answer = question.nextElementSibling;
    if(question.classList.contains("active")){
      answer.style.maxHeight = answer.scrollHeight + "px";
    } else {
      answer.style.maxHeight = 0;
    }
  })
})


function activeLink(){
    
    list.forEach((item) =>  item.classList.remove('selected'))
    var i = 0
    // Separates string by "&" selects string of index 0 and adds "&gray.png" at the end
    // to change the icon on navbar
    for(var item of list){
        if (i != 0){
            item.querySelector('a .icon img').src =
            item.querySelector('a .icon img').src.split("&")[0] + "&white.png"
        }
        i++;
    }

    this.classList.add('selected');
    // Separates string by "&" selects string of index 0 and adds "&gray.png" at the end
    // to change the icon on navbar
    this.querySelector('a .icon img').src =
    this.querySelector('a .icon img').src.split("&")[0] + "&gray.png"
}

// Collapse Status Navbar
var navBarStatus = 0;
function navbarCollapse(){
    document.querySelectorAll(".navigation").forEach(function(element) {
        buttonIconOne = document.querySelector(".top-menu button img:nth-child(1)")
        itemList = document.querySelectorAll(".navigation ul li")
        mainPage = document.querySelector(".main")
        brand = document.querySelector(".brand")
        brandName = document.querySelector(".brand-name")
        
        if(navBarStatus == 0){
            //Change NavMenu
            element.style.width = "90px";
            
            //Change Items menu
            itemList.forEach(function(element){
                element.style.width = "70px"
            })

            //Change Brand
            brand.style.width = "70px"
            brandName.style.display = "none";

            //Change main
            mainPage.style.width = "calc(100% - 90px)"
            mainPage.style.left = "90px"

            //Change button icon
            buttonIconOne.style.transform = 'rotate(-90deg)'
            navBarStatus = 1;
        }else{
            //Change NavMenu
            element.style.width = "300px";

            //Change Items menu
            itemList.forEach(function(element){
                element.style.width = "280px"
            })

            //Change Brand
            brand.style.width = "280px"
            brandName.style.display = "inline-flex";


            //Change main
            mainPage.style.width = "calc(100% - 300px)"
            mainPage.style.left = "300px"

            //Change button icon
            buttonIconOne.style.transform = 'rotate(90deg)'
            navBarStatus = 0;
        }
    });
}

//Draw Charts
//Ganho Ano Chart

const ganhoAnoCtx = document.getElementById('ganhoAnoChart');
const ganhoAnoChart = new Chart(ganhoAnoCtx, {
    type: 'line',
    data: {
        labels: [2017, 2018, 2019, 2020, 2021, 2022],
        datasets: [{
            label: 'Ganho em Euros',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(100, 100, 255, 0.5)'
            ],
            borderColor: [
                'rgba(30, 0, 255, 0.8)'
            ],
            borderWidth: 1
        }]
    },
    plugins: {
        title: {
            display: true,
            text: 'ashdujashdasudhasiuydhas'
        }
    },
    options: {
        maintainAspectRatio: false,
        responsive:true
    }
});


const clientesAgenteCtx = document.getElementById('clientesAgenteChart');
const clientesAgenteChart = new Chart(clientesAgenteCtx, {
    type: 'bar',
    data: {
        labels: ['João', 'Catarina', 'António', 'Manuel', 'Pedro'],
        datasets: [{
            label: 'Número de Clientes',
            data: [12, 19, 3, 5, 10],
            backgroundColor: [
                'rgb(245, 164, 66)',
                'rgb(245, 164, 66)',
                'rgb(245, 164, 66)',
                'rgb(245, 164, 66)',
                'rgb(245, 164, 66)'
            ],
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        },
        maintainAspectRatio: false,
        responsive:true
        
    }
});

const baseCoberturaId = "5";

function deleteCobertura(id){
    var cobertura = document.getElementById('cobertura-'+id);
    cobertura.remove()
}

function addCobertura(){
    var coberturas = document.getElementById('coberturas-list');
    var lastChildId = coberturas.childElementCount;
    console.log(coberturas.lastChild);
    var new_id = parseInt(lastChildId)+1;
    var li = document. createElement("li");
    li.id='cobertura-'+new_id.toString();
    var id = document.createElement("SPAN");
    id.classList.add('Id');
    id.innerText = new_id.toString();
    li.appendChild(id);
    var seguro = document.createElement("SPAN");
    seguro.classList.add('Seguro');
    seguro.innerText = 'Veiculo';
    li.appendChild(seguro);
    var nome = document.createElement("SPAN");
    nome.classList.add('Nome');
    nome.innerText = 'CoberturaDummy Text';
    li.appendChild(nome);
    var event = document.createElement("SPAN");
    event.classList.add('form-icon');
    var icon = document.createElement('img');
    icon.src= 'icons/close_round.png';
    event.appendChild(icon);
    event.onclick = function () {deleteCobertura(new_id.toString())};
    li.appendChild(event);
    coberturas.appendChild(li);
}


//Gestão de Agente ---------------------------------------------------------

function funcGestorAdd(){
    var id = 0
    var nome = document.getElementById("agente-nome-id").value;
    var email = document.getElementById("agente-email-id").value;
    var nascimento = document.getElementById("agente-nascimento-id").value;
    var morada = document.getElementById("agente-morada-id").value;
    var contacto = document.getElementById("agente-contacto-id").value;

    //post request

    var newDiv = "<li id='agente-list-" + id + "'>" + 
    "<span class='Id'>"+ id +"</span>" +
    "<span class='Seguro'>"+ nome +"</span>" +
    "<span class='Nome'>"+ email +"</span>" +
    "<span class='form-icon' onclick='FuncAgenteEditSelect("+ id +")'><img src='icons/ball_point_pen&gray.png'></span>" +
    "<span class='form-icon' onclick='FuncAgenteDelete("+ id +")'><img src='icons/close_round.png'></span></li>"

    document.getElementById("coberturas-list").innerHTML += (newDiv)

    document.getElementById("agente-nome-id").value = null;
    document.getElementById("agente-email-id").value = null;
    document.getElementById("agente-nascimento-id").value = null;
    document.getElementById("agente-morada-id").value = null;
    document.getElementById("agente-contacto-id").value = null;
}

function funcAgenteAdd(){
    id = 2

    var nome = document.getElementById("agente-nome-id").value;
    var email = document.getElementById("agente-email-id").value;
    var nascimento = document.getElementById("agente-nascimento-id").value;
    var morada = document.getElementById("agente-morada-id").value;
    var contacto = document.getElementById("agente-contacto-id").value;

    //post request

    var newDiv = "<li id='agente-list-" + id + "'>" + 
    "<span class='Id'>"+ id +"</span>" +
    "<span class='Seguro'>"+ nome +"</span>" +
    "<span class='Nome'>"+ email +"</span>" +
    "<span class='form-icon' onclick='FuncAgenteEditSelect("+ id +")'><img src='icons/ball_point_pen&gray.png'></span>" +
    "<span class='form-icon' onclick='FuncAgenteDelete("+ id +")'><img src='icons/close_round.png'></span></li>"

    document.getElementById("coberturas-list").innerHTML += (newDiv)

    document.getElementById("agente-nome-id").value = null;
    document.getElementById("agente-email-id").value = null;
    document.getElementById("agente-nascimento-id").value = null;
    document.getElementById("agente-morada-id").value = null;
    document.getElementById("agente-contacto-id").value = null;
}

function funcAgenteEdit(){

    document.getElementById("agenteAdd").style.display = "block"
    document.getElementById("agenteEdit").style.display = "none"

    var id  = document.getElementById("agente-id").value;
    FuncAgenteDelete(id)

    var nome = document.getElementById("agente-nome-id").value;
    var email = document.getElementById("agente-email-id").value;
    var nascimento = document.getElementById("agente-nascimento-id").value;
    var morada = document.getElementById("agente-morada-id").value;
    var contacto = document.getElementById("agente-contacto-id").value;

    //put request

    var newDiv = "<li id='agente-list-" + id + "'>" + 
    "<span class='Id'>"+ id +"</span>" +
    "<span class='Seguro'>"+ nome +"</span>" +
    "<span class='Nome'>"+ email +"</span>" +
    "<span class='form-icon' onclick='FuncAgenteEditSelect("+ id +")'><img src='icons/ball_point_pen&gray.png'></span>" +
    "<span class='form-icon' onclick='FuncAgenteDelete("+ id +")'><img src='icons/close_round.png'></span></li>"

    document.getElementById("coberturas-list").innerHTML += (newDiv)

    document.getElementById("agente-nome-id").value = null;
    document.getElementById("agente-email-id").value = null;
    document.getElementById("agente-nascimento-id").value = null;
    document.getElementById("agente-morada-id").value = null;
    document.getElementById("agente-contacto-id").value = null;
}

function FuncAgenteEditSelect(id){
    
    document.getElementById("agenteAdd").style.display = "none"
    document.getElementById("agenteEdit").style.display = "block"

    document.getElementById("agente-id").value = id;

    //get request by id
    
    var req = {
        "nome": "Ex Nome",
        "email": "email@email.com",
        "nascimento": "2001-01-01",
        "morada": "Ex Morada",
        "contacto": "951222333"
    }

    document.getElementById("agente-nome-id").value = req.nome;
    document.getElementById("agente-email-id").value = req.email;
    document.getElementById("agente-nascimento-id").value = req.nascimento;
    document.getElementById("agente-morada-id").value = req.morada;
    document.getElementById("agente-contacto-id").value = req.contacto;
}

function FuncAgenteDelete(id){
    var agente = document.getElementById("agente-list-" + id);

    // delete request

    agente.remove()
}

//--------------------------------------------------------------------------
//Gestão de Seguros --------------------------------------------------------

function funcSeguroAdd(){
    var id = 0
    var tipo = document.getElementById("seguro-tipo-id").value;
    var subtipo = document.getElementById("seguro-subtipo-id").value;

    //post request

    var newDiv = "<li id='seguro-list-" + id + "'>" + 
    "<span class='Id'>"+ id +"</span>" +
    "<span class='Seguro'>"+ tipo +"</span>" +
    "<span class='Nome'>"+ subtipo +"</span>" +
    "<span class='form-icon' onclick='FuncSeguroDelete("+ id +")'><img src='icons/close_round.png'></span></li>"

    document.getElementById("coberturas-list").innerHTML += (newDiv)
}

function FuncSeguroDelete(id){
    var seguro = document.getElementById("seguro-list-" + id);

    // delete request

    seguro.remove()
}

//--------------------------------------------------------------------------
//Gestão de Apolices --------------------------------------------------------

function FuncApoliceShow(id){
    document.getElementById("apolice-id").value = id;

    //get request by id

    switch (id){
        case 1:
            var req = {
                "nome": "Kia Picanto",
                "tipo": "Seguro Automóvel",
                "data": "2020-05-21",
                "valor": "150.15"
            }
            break;
        
        case 2:
            var req = {
                "nome": "Leonel Antunes",
                "tipo": "Seguro de Vida",
                "data": "2022-08-31",
                "valor": "300.50"
            }
            break;
            
        case 3:
            var req = {
                "nome": "Citroen Saxo",
                "tipo": "Seguro Automóvel",
                "data": "2023-01-13",
                "valor": "99.99"
            }
    }

    document.getElementById("apolice-nome-id").value = req.nome;
    document.getElementById("apolice-tipo-id").value = req.tipo;
    document.getElementById("apolice-data-id").value = req.data;
    document.getElementById("apolice-valor-id").value = req.valor + "€";
}

function funcApoliceRemove(){
    var id = document.getElementById("apolice-id").value;

    //Remove request

    var apolice = document.getElementById("apolice-list-" + id);
    apolice.remove()
}
//--------------------------------------------------------------------------
