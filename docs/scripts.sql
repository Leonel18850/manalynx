
-- DROP DATABASE ManaLynx;
-- CREATE DATABASE ManaLynx;
-- CREATE SCHEMA Manalynx

-- -----------------------------------------------------
-- Table `ManaLynx`.`Pessoa`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Pessoa (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  Nome VARCHAR(45) NULL,
  DataNascimento DATE NULL,
  Nacionalidade VARCHAR(45) NULL,
  CC VARCHAR(45) NULL,
  ValidadeCC DATE NULL,
  NIF VARCHAR(10) NULL,
  NSS VARCHAR(12) NULL,
  NUS VARCHAR(10) NULL,
  EstadoCivil nvarchar(45) check (EstadoCivil in ('Solteiro', 'Casado', 'Viuvo', 'Uniao de Facto')) NULL
);

-- -----------------------------------------------------
-- Table `ManaLynx`.`Agente`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Agente (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  NAgente INT NOT NULL,
  PessoaId INT FOREIGN KEY REFERENCES Manalynx.Pessoa(Id),
  -- EquipaId INT FOREIGN KEY REFERENCES Manalynx.Equipa(Id)
);

-- -----------------------------------------------------
-- Table `ManaLynx`.`Gestor`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Gestor (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  AgenteId INT FOREIGN KEY REFERENCES Manalynx.Agente(Id)
);

-- -----------------------------------------------------
-- Table `ManaLynx`.`Equipa`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Equipa (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  Nome VARCHAR(255) NOT NULL,
  Regiao VARCHAR(127) NOT NULL,
  GestorId INT FOREIGN KEY REFERENCES Manalynx.Gestor(Id)
);

ALTER TABLE Manalynx.Agente
  ADD EquipaId INT FOREIGN KEY REFERENCES Manalynx.Equipa(Id)
;


-- -----------------------------------------------------
-- Table `ManaLynx`.`LoginCredential`
-- -----------------------------------------------------
CREATE TABLE Manalynx.LoginCredential (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  ManaHash VARCHAR(255),
  ManaSalt VARCHAR(255)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`User`
-- -----------------------------------------------------
CREATE TABLE Manalynx.ManaUser (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  Username VARCHAR(45) NOT NULL,
  Email VARCHAR(45) NOT NULL,
  UserRole nvarchar(45) check (UserRole in ('Admin', 'Gestor', 'Agente', 'Cliente')) NOT NULL,
  PessoaId INT FOREIGN KEY REFERENCES Manalynx.Pessoa(Id),
  LoginCredential INT FOREIGN KEY REFERENCES Manalynx.LoginCredential(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`DadoClinico`
-- -----------------------------------------------------
CREATE TABLE Manalynx.DadoClinico (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  Altura FLOAT NULL,
  Peso FLOAT NULL,
  Tensao nvarchar(45) check (Tensao in ('Normal', 'Hipertenso', 'Hipotenso')) NULL
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`Seguro`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Seguro (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  Nome VARCHAR(45) NOT NULL,
  Ativo BIT NOT NULL,
  Tipo nvarchar(45) check (Tipo in ('Saude', 'Vida', 'Veiculo')) NOT NULL
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`Coberturas`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Cobertura (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  DescricaoCobertura VARCHAR(45) NOT NULL,
  SeguroId INT FOREIGN KEY REFERENCES Manalynx.Seguro(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`Tratamento`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Tratamento (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  NomeTratamento VARCHAR(45) NULL,
  Frequencia VARCHAR(45) NULL,
  UltimaToma DATE NULL,
  DadoClinicoId INT FOREIGN KEY REFERENCES Manalynx.DadoClinico(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`Doencas`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Doenca (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  NomeDoenca VARCHAR(45) NOT NULL,
  Descricao VARCHAR(45) NULL
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`DadosClinicosHasDoencas`
-- -----------------------------------------------------
CREATE TABLE Manalynx.DadosClinicoHasDoenca (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  DadoClinicoId INT FOREIGN KEY REFERENCES Manalynx.DadoClinico(Id),
  DoencaId INT FOREIGN KEY REFERENCES Manalynx.Doenca(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`CategoriaVeiculo`
-- -----------------------------------------------------
CREATE TABLE Manalynx.CategoriaVeiculo (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  Categoria VARCHAR(45) NOT NULL
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`Cliente`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Cliente (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  Profissao VARCHAR(45) NULL,
  ProfissaoRisco BIT NULL,
  PessoaId INT FOREIGN KEY REFERENCES Manalynx.Pessoa(Id),
  DadoClinicoId INT FOREIGN KEY REFERENCES Manalynx.DadoClinico(Id),
  AgenteID INT FOREIGN KEY REFERENCES Manalynx.Agente(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`Veiculo`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Veiculo (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  VIN VARCHAR(17) NOT NULL,
  Matricula VARCHAR(8) NOT NULL,
  Ano INT NOT NULL,
  Mes INT NOT NULL,
  Marca VARCHAR(45) NOT NULL,
  Modelo VARCHAR(45) NOT NULL,
  Cilindrada INT NULL,
  Portas INT NULL,
  Lugares INT NULL,
  Potencia INT NULL,
  Peso INT NULL,
  CategoriaVeiculoId INT FOREIGN KEY REFERENCES Manalynx.CategoriaVeiculo(Id),
  ClienteId INT FOREIGN KEY REFERENCES Manalynx.Cliente(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`Apolice`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Apolice (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  Ativa BIT NOT NULL,
  Premio FLOAT NULL,
  Validade DATE NULL,
  Fracionamento nvarchar(45) check (Fracionamento in ('Mensal', 'Trimestral', 'Semestral', 'Anual')) NULL,
  SeguroId INT FOREIGN KEY REFERENCES Manalynx.Seguro(Id),
  AgenteId INT FOREIGN KEY REFERENCES Manalynx.Agente(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`Pagamento`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Pagamento (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  Metodo VARCHAR(45) NOT NULL,
  DataEmissao DATE NOT NULL,
  DataPagamento DATE NOT NULL,
  Montante FLOAT NOT NULL,
  ApoliceId INT FOREIGN KEY REFERENCES Manalynx.Apolice(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`CoberturaHasApolice`
-- -----------------------------------------------------
CREATE TABLE Manalynx.CoberturaHasApolice (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  CoberturaId INT FOREIGN KEY REFERENCES Manalynx.Cobertura(Id),
  ApoliceId INT FOREIGN KEY REFERENCES Manalynx.Apolice(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`ApoliceSaude`
-- -----------------------------------------------------
CREATE TABLE Manalynx.ApoliceSaude (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  ClienteId INT FOREIGN KEY REFERENCES Manalynx.Cliente(Id),
  ApoliceId INT FOREIGN KEY REFERENCES Manalynx.Apolice(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`Transacao`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Transacao (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  Descricao VARCHAR(45) NOT NULL,
  DataTransacao DATE NOT NULL,
  ApoliceSaudeId INT FOREIGN KEY REFERENCES Manalynx.ApoliceSaude(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`Contacto`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Contacto (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  Valor VARCHAR(45) NOT NULL,
  Tipo nvarchar(45) check (Tipo in ('Telemovel', 'Email', 'Telefone')) NOT NULL,
  PessoaId INT FOREIGN KEY REFERENCES Manalynx.Pessoa(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`ApoliceVeiculo`
-- -----------------------------------------------------
CREATE TABLE Manalynx.ApoliceVeiculo (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  ApoliceId INT FOREIGN KEY REFERENCES Manalynx.Apolice(Id),
  VeiculoId INT FOREIGN KEY REFERENCES Manalynx.Veiculo(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`Sinistro`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Sinistro (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  Descricao VARCHAR(45) NOT NULL,
  Estado nvarchar(45) check (Estado in ('Aguardar Validação ','Aguardar Provas', 'Aguardar Peritagem', 'Resultado Emitido')) NOT NULL,
  Reembolso FLOAT NULL,
  DataSinistro DATE NOT NULL,
  Valido BIT NULL,
  Deferido BIT NULL,
);

CREATE TABLE Manalynx.Prova(
 Id INT PRIMARY KEY IDENTITY(1, 1),
 Conteudo VARCHAR(100) NULL,
 DataSubmissao DATE NOT NULL,
 SinistroId INT FOREIGN KEY REFERENCES Manalynx.Sinistro(Id),
);

CREATE TABLE Manalynx.RelatorioPeritagem(
 Id INT PRIMARY KEY IDENTITY(1, 1),
 Conteudo VARCHAR(100) NULL,
 DataRelatorio DATE NOT NULL,
 Deferido BIT NOT NULL,
 SinistroId INT FOREIGN KEY REFERENCES Manalynx.Sinistro(Id),
)

-- -----------------------------------------------------
-- Table `ManaLynx`.`ApolicePessoal`
-- -----------------------------------------------------
CREATE TABLE Manalynx.ApolicePessoal (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  ApoliceId INT FOREIGN KEY REFERENCES Manalynx.Apolice(Id),
  ClienteId INT FOREIGN KEY REFERENCES Manalynx.Cliente(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`SinistroPessoal`
-- -----------------------------------------------------
CREATE TABLE Manalynx.SinistroPessoal (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  ApolicePessoalId INT FOREIGN KEY REFERENCES Manalynx.ApolicePessoal(Id),
  SinistroId INT FOREIGN KEY REFERENCES Manalynx.Sinistro(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`SinistroVeiculo`
-- -----------------------------------------------------
CREATE TABLE Manalynx.SinistroVeiculo (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  SinistroId INT FOREIGN KEY REFERENCES Manalynx.Sinistro(Id),
  ApoliceVeiculoId INT FOREIGN KEY REFERENCES Manalynx.ApoliceVeiculo(Id)
);


-- -----------------------------------------------------
-- Table `ManaLynx`.`Logs`
-- -----------------------------------------------------
CREATE TABLE Manalynx.Logs (
  Id INT PRIMARY KEY IDENTITY(1, 1),
  LogDate TIMESTAMP NULL,
  Username VARCHAR(45) NULL,
  Query VARCHAR(45) NULL,
);
