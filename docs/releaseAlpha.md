# ManaLynx

<img src="https://i.imgur.com/aDHj38L.png" width="650" height="650" />

## Elementos

- **Product Owner** - Miguel Silva Nº18829

- **Scrum Master** &nbsp;&nbsp;- Leonel Fernandes Nº18850

- **Developer** - André Cardoso Nº18848

- **Developer** - José Cosgrove Nº18826

<div style="page-break-after: always;"></div>

## Introdução

ManaLynx é uma plataforma que procura simplificar a vida dos agentes de seguros, permitindo ao agente fazer todo o processo de emissão de apolice num único sítio! 
A plataforma WEB procura também facilitar aos clientes o pedido de simulações e gestão das mesmas desde a fase de ativação até a gestão de ocorrências/sinistros. 
Além do descrito também é possível efetuar a gestão das equipas de agentes através de um dashboard dinâmico onde encontra tudo o necessário para levar a sua equipa ao sucesso.   

## Requisitos

### Requisitos Funcionais

Em todos os projetos de engenharia de software é necessária a definição de requisitos, de forma a tornar mais fácil a fase de implementação do projeto visto que são enunciadas todas as informações necessárias para determinar as funcionalidades principais do projeto.

Aqui estão definidos os requisitos Funcionais categorizados por tipo de utilizador:

#### Geral

* Autenticação

#### Gestor de Equipa

* CRUD de Agentes
* CRUD de Seguros praticados
* Análise de Estatísticas
  * Filtros de Dados
  * Núm. de Apólices
  * Cashflow
  * etc.

#### Agente de Seguros

* CRUD de Clientes
* CRUD de Apólices
* CRUD de Leads(Possíveis Clientes)
* Processar Sinistros

#### Cliente

* Registo/Eliminação de Conta
* Consulta de Seguros disponíveis
* Simulação de Seguros
* 'Ativação de Seguros'
* Consulta de Apólices Ativas/Não Ativas
* Submeter e Verificar Estado de Sinistros

<br/>

### Requisitos Não Funcionais

Os requisitos não-funcionais (RNF), são essenciais para a elaboração de um projeto, uma vez que tendem a dar resposta a pontos cruciais.

Os RNF definidos para este projeto são os seguintes:

- Implementação, devido à divisão entre back-end e front-end, as tecnologias a serem utilizadas serão, respetivamente, .Net e Angular.
- Interoperabilidade, uma vez que este sistema deve conseguir comunicar com uma Base de Dados online (Azure MS SQL).
- Ético, uma vez que são tratados dados sensíveis, devemos apenas permitir que um determinado utilizador seja capaz de aceder a determinada informação.
- Segurança, implementar logs para chamadas e acessos à Base de Dados, salvaguardando que os dados podem novamente ser repostos. Os dados armazenados na Base de Dados devem ainda estar encriptados.
- Disponibilidade, qualquer alteração que implique que o sistema fique indisponivel deve ser executado num horário que interfica com o menor número de clientes possíveis (preferencialmente entre as 02h00 e as 06h00).

<div style="page-break-after: always;"></div>

## Diagramas

### Use Case Diagram

![](UseCase.png)

<div style="page-break-after: always;"></div>

### Activity Diagram

#### A - Simulação Seguro

![](https://i.imgur.com/Wu2Omtn.png)

<div style="page-break-after: always;"></div>

#### B - Sinistro

![](Activity_Diagram.jpg)

<div style="page-break-after: always;"></div>

### Sequence Diagram

#### A - Simulação Seguro

![](https://i.imgur.com/6I0x4Cz.png)

<div style="page-break-after: always;"></div>

#### B - Sinistro

![](Sinistro%20SD.jpg)

<!-- mudar aqui o diagrama-->

<div style="page-break-after: always;"></div>

### Class Diagram

![](ManaLynx%20ClassDiagram.jpg)

<div style="page-break-after: always;"></div>

### ER Diagram

![](ER%20Diagram.png)

<div style="page-break-after: always;"></div>

### Interaction Overview Diagram

<br/>
<br/>

![](https://i.imgur.com/UpehQrR.png)

<div style="page-break-after: always;"></div>

### Component Diagram

#### Simulação de Seguro

![](https://i.imgur.com/jwedAeo.png)

## Mockups

Os Mockups do sistema servem de representação do produto final. Diversos elementos presentes na interface poderão ser objetos de mudança até à última release do projeto.

A interface aqui desenhada tenta ir de acordo com os Requisitos Funcionais definidos, assim como todos os diagramas que definem o flow da aplicação.

<div style="page-break-after: always;"></div>

### Todos os Utilizadores

#### Login

![](https://i.imgur.com/2eyTnVP.png)

#### Dados Pessoais

![](https://i.imgur.com/VH56QWq.png)

<div style="page-break-after: always;"></div>

### Cliente

#### Dashboard

![](https://i.imgur.com/VghX3ny.png)

#### Gestão de Apólices

![](https://i.imgur.com/7d7ItdC.png)

<div style="page-break-after: always;"></div>

### Cliente

#### Pedidos de Simulação

![](https://i.imgur.com/1VpHQjg.png)

<div style="page-break-after: always;"></div>

### Agente de Seguros

#### Dashboard

![](https://i.imgur.com/zEqS0im.png)

#### Aprovação de Simulação

![](https://i.imgur.com/X5D7I34.png)

<div style="page-break-after: always;"></div>

#### Registo de Leads

![](https://i.imgur.com/CLJxUMr.png)

<div style="page-break-after: always;"></div>

### Gestor de Equipa

#### Dashboard

![](https://i.imgur.com/4YoU0fU.png)

#### Gestão de Agentes

![](https://i.imgur.com/c6Hi7kS.png)

<div style="page-break-after: always;"></div>

#### Gestão de Seguros

![](https://i.imgur.com/5cdhlJ3.png)

<div style="page-break-after: always;"></div>

## Bugs conhecidos

1. Não existe verificação de update para quando a apólice já está em pagamento emitido

2. Loop na conversão das classes para JSON em logs.

3. Dado Clinico Post deve ser executado apenas quando um cliente novo fizer post

4. Considerar fazer verificações na criação de apolices acerca dos cliente id 

5. Rota create/update agente [Admin] caso id equipa nao seja valido exception

6. Rota create/update cobertura [Admin] caso id seguro seja invalido exception

7. Rota create/update Gestor [Admin] caso id equipa seja invalido exception

8. Rotas Simulação CalculatePremio, não tem consideração coberturas

9. Login ignora letras maiúsculas

10. Não existe validação se as coberturas são do tipo de seguro da apólices

11. Não existe validação se a apolice criada é do tipo de seguro possivel para esse tipo de apolice

12. Rota ApolicePessoal/AskSimulacao falha a ler bearer token

## Aspetos a Melhorar

1. Estrutura do código
2. Implementação de Unit Tests
3. Documentação do código
4. Otimização das Verificações Automáticas feitas  pelo Sistema
