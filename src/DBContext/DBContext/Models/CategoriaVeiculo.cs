﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class CategoriaVeiculo
    {
        public CategoriaVeiculo()
        {
            Veiculos = new HashSet<Veiculo>();
        }

        public int Id { get; set; }
        public string Categoria { get; set; } = null!;

        public virtual ICollection<Veiculo> Veiculos { get; set; }
    }
}
