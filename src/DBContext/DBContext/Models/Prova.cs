﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Prova
    {
        public int Id { get; set; }
        public string? Conteudo { get; set; }
        public DateTime DataSubmissao { get; set; }
        public int? SinistroId { get; set; }

        public virtual Sinistro? Sinistro { get; set; }
    }
}
