﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Gestor
    {
        public Gestor()
        {
            Equipas = new HashSet<Equipa>();
        }

        public int Id { get; set; }
        public int? AgenteId { get; set; }

        public virtual Agente? Agente { get; set; }
        public virtual ICollection<Equipa> Equipas { get; set; }
    }
}
