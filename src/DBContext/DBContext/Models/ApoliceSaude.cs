﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class ApoliceSaude
    {
        public ApoliceSaude()
        {
            Transacaos = new HashSet<Transacao>();
        }

        public int Id { get; set; }
        public int? ClienteId { get; set; }
        public int? ApoliceId { get; set; }

        public virtual Apolice? Apolice { get; set; }
        public virtual Cliente? Cliente { get; set; }
        public virtual ICollection<Transacao> Transacaos { get; set; }
    }
}
