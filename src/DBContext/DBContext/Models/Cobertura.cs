﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Cobertura
    {
        public Cobertura()
        {
            CoberturaHasApolices = new HashSet<CoberturaHasApolice>();
        }

        public int Id { get; set; }
        public string DescricaoCobertura { get; set; } = null!;
        public int? SeguroId { get; set; }

        public virtual Seguro? Seguro { get; set; }
        public virtual ICollection<CoberturaHasApolice> CoberturaHasApolices { get; set; }
    }
}
