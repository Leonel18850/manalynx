﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Doenca
    {
        public Doenca()
        {
            DadosClinicoHasDoencas = new HashSet<DadosClinicoHasDoenca>();
        }

        public int Id { get; set; }
        public string NomeDoenca { get; set; } = null!;
        public string? Descricao { get; set; }

        public virtual ICollection<DadosClinicoHasDoenca> DadosClinicoHasDoencas { get; set; }
    }
}
