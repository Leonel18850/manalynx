﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Sinistro
    {
        public Sinistro()
        {
            Provas = new HashSet<Prova>();
            RelatorioPeritagems = new HashSet<RelatorioPeritagem>();
            SinistroPessoals = new HashSet<SinistroPessoal>();
            SinistroVeiculos = new HashSet<SinistroVeiculo>();
        }

        public int Id { get; set; }
        public string Descricao { get; set; } = null!;
        public string Estado { get; set; } = null!;
        public double? Reembolso { get; set; }
        public DateTime DataSinistro { get; set; }
        public bool? Valido { get; set; }
        public bool? Deferido { get; set; }

        public virtual ICollection<Prova> Provas { get; set; }
        public virtual ICollection<RelatorioPeritagem> RelatorioPeritagems { get; set; }
        public virtual ICollection<SinistroPessoal> SinistroPessoals { get; set; }
        public virtual ICollection<SinistroVeiculo> SinistroVeiculos { get; set; }
    }
}
