﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DBContext.Models
{
    public partial class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Agente> Agentes { get; set; } = null!;
        public virtual DbSet<Apolice> Apolices { get; set; } = null!;
        public virtual DbSet<ApolicePessoal> ApolicePessoals { get; set; } = null!;
        public virtual DbSet<ApoliceSaude> ApoliceSaudes { get; set; } = null!;
        public virtual DbSet<ApoliceVeiculo> ApoliceVeiculos { get; set; } = null!;
        public virtual DbSet<CategoriaVeiculo> CategoriaVeiculos { get; set; } = null!;
        public virtual DbSet<Cliente> Clientes { get; set; } = null!;
        public virtual DbSet<Cobertura> Coberturas { get; set; } = null!;
        public virtual DbSet<CoberturaHasApolice> CoberturaHasApolices { get; set; } = null!;
        public virtual DbSet<Contacto> Contactos { get; set; } = null!;
        public virtual DbSet<DadoClinico> DadoClinicos { get; set; } = null!;
        public virtual DbSet<DadosClinicoHasDoenca> DadosClinicoHasDoencas { get; set; } = null!;
        public virtual DbSet<Doenca> Doencas { get; set; } = null!;
        public virtual DbSet<Equipa> Equipas { get; set; } = null!;
        public virtual DbSet<Gestor> Gestors { get; set; } = null!;
        public virtual DbSet<Log> Logs { get; set; } = null!;
        public virtual DbSet<LoginCredential> LoginCredentials { get; set; } = null!;
        public virtual DbSet<ManaUser> ManaUsers { get; set; } = null!;
        public virtual DbSet<Pagamento> Pagamentos { get; set; } = null!;
        public virtual DbSet<Pessoa> Pessoas { get; set; } = null!;
        public virtual DbSet<Seguro> Seguros { get; set; } = null!;
        public virtual DbSet<Sinistro> Sinistros { get; set; } = null!;
        public virtual DbSet<SinistroPessoal> SinistroPessoals { get; set; } = null!;
        public virtual DbSet<SinistroVeiculo> SinistroVeiculos { get; set; } = null!;
        public virtual DbSet<Transacao> Transacaos { get; set; } = null!;
        public virtual DbSet<Tratamento> Tratamentos { get; set; } = null!;
        public virtual DbSet<Veiculo> Veiculos { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=tcp:manalynx.database.windows.net,1433;Database=ManaLynx;Initial Catalog=ManaLynx;Persist Security Info=False;User ID=manalynxadmin;Password=ManaLynx09?;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Agente>(entity =>
            {
                entity.ToTable("Agente", "Manalynx");

                entity.Property(e => e.Nagente).HasColumnName("NAgente");

                entity.HasOne(d => d.Equipa)
                    .WithMany(p => p.Agentes)
                    .HasForeignKey(d => d.EquipaId)
                    .HasConstraintName("FK__Agente__EquipaId__634EBE90");

                entity.HasOne(d => d.Pessoa)
                    .WithMany(p => p.Agentes)
                    .HasForeignKey(d => d.PessoaId)
                    .HasConstraintName("FK__Agente__PessoaId__5CA1C101");
            });

            modelBuilder.Entity<Apolice>(entity =>
            {
                entity.ToTable("Apolice", "Manalynx");

                entity.Property(e => e.Fracionamento).HasMaxLength(45);

                entity.Property(e => e.Validade).HasColumnType("date");

                entity.HasOne(d => d.Agente)
                    .WithMany(p => p.Apolices)
                    .HasForeignKey(d => d.AgenteId)
                    .HasConstraintName("FK__Apolice__AgenteI__09746778");

                entity.HasOne(d => d.Seguro)
                    .WithMany(p => p.Apolices)
                    .HasForeignKey(d => d.SeguroId)
                    .HasConstraintName("FK__Apolice__SeguroI__0880433F");
            });

            modelBuilder.Entity<ApolicePessoal>(entity =>
            {
                entity.ToTable("ApolicePessoal", "Manalynx");

                entity.HasOne(d => d.Apolice)
                    .WithMany(p => p.ApolicePessoals)
                    .HasForeignKey(d => d.ApoliceId)
                    .HasConstraintName("FK__ApolicePe__Apoli__24285DB4");

                entity.HasOne(d => d.Cliente)
                    .WithMany(p => p.ApolicePessoals)
                    .HasForeignKey(d => d.ClienteId)
                    .HasConstraintName("FK__ApolicePe__Clien__251C81ED");
            });

            modelBuilder.Entity<ApoliceSaude>(entity =>
            {
                entity.ToTable("ApoliceSaude", "Manalynx");

                entity.HasOne(d => d.Apolice)
                    .WithMany(p => p.ApoliceSaudes)
                    .HasForeignKey(d => d.ApoliceId)
                    .HasConstraintName("FK__ApoliceSa__Apoli__13F1F5EB");

                entity.HasOne(d => d.Cliente)
                    .WithMany(p => p.ApoliceSaudes)
                    .HasForeignKey(d => d.ClienteId)
                    .HasConstraintName("FK__ApoliceSa__Clien__12FDD1B2");
            });

            modelBuilder.Entity<ApoliceVeiculo>(entity =>
            {
                entity.ToTable("ApoliceVeiculo", "Manalynx");

                entity.HasOne(d => d.Apolice)
                    .WithMany(p => p.ApoliceVeiculos)
                    .HasForeignKey(d => d.ApoliceId)
                    .HasConstraintName("FK__ApoliceVe__Apoli__1D7B6025");

                entity.HasOne(d => d.Veiculo)
                    .WithMany(p => p.ApoliceVeiculos)
                    .HasForeignKey(d => d.VeiculoId)
                    .HasConstraintName("FK__ApoliceVe__Veicu__1E6F845E");
            });

            modelBuilder.Entity<CategoriaVeiculo>(entity =>
            {
                entity.ToTable("CategoriaVeiculo", "Manalynx");

                entity.Property(e => e.Categoria)
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.ToTable("Cliente", "Manalynx");

                entity.Property(e => e.AgenteId).HasColumnName("AgenteID");

                entity.Property(e => e.Profissao)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.HasOne(d => d.Agente)
                    .WithMany(p => p.Clientes)
                    .HasForeignKey(d => d.AgenteId)
                    .HasConstraintName("FK__Cliente__AgenteI__00DF2177");

                entity.HasOne(d => d.DadoClinico)
                    .WithMany(p => p.Clientes)
                    .HasForeignKey(d => d.DadoClinicoId)
                    .HasConstraintName("FK__Cliente__DadoCli__7FEAFD3E");

                entity.HasOne(d => d.Pessoa)
                    .WithMany(p => p.Clientes)
                    .HasForeignKey(d => d.PessoaId)
                    .HasConstraintName("FK__Cliente__PessoaI__7EF6D905");
            });

            modelBuilder.Entity<Cobertura>(entity =>
            {
                entity.ToTable("Cobertura", "Manalynx");

                entity.Property(e => e.DescricaoCobertura)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.HasOne(d => d.Seguro)
                    .WithMany(p => p.Coberturas)
                    .HasForeignKey(d => d.SeguroId)
                    .HasConstraintName("FK__Cobertura__Segur__719CDDE7");
            });

            modelBuilder.Entity<CoberturaHasApolice>(entity =>
            {
                entity.ToTable("CoberturaHasApolice", "Manalynx");

                entity.HasOne(d => d.Apolice)
                    .WithMany(p => p.CoberturaHasApolices)
                    .HasForeignKey(d => d.ApoliceId)
                    .HasConstraintName("FK__Cobertura__Apoli__10216507");

                entity.HasOne(d => d.Cobertura)
                    .WithMany(p => p.CoberturaHasApolices)
                    .HasForeignKey(d => d.CoberturaId)
                    .HasConstraintName("FK__Cobertura__Cober__0F2D40CE");
            });

            modelBuilder.Entity<Contacto>(entity =>
            {
                entity.ToTable("Contacto", "Manalynx");

                entity.Property(e => e.Contacto1)
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasColumnName("Contacto");

                entity.Property(e => e.Tipo).HasMaxLength(45);

                entity.HasOne(d => d.Pessoa)
                    .WithMany(p => p.Contactos)
                    .HasForeignKey(d => d.PessoaId)
                    .HasConstraintName("FK__Contacto__Pessoa__1A9EF37A");
            });

            modelBuilder.Entity<DadoClinico>(entity =>
            {
                entity.ToTable("DadoClinico", "Manalynx");

                entity.Property(e => e.Tensao).HasMaxLength(45);
            });

            modelBuilder.Entity<DadosClinicoHasDoenca>(entity =>
            {
                entity.ToTable("DadosClinicoHasDoenca", "Manalynx");

                entity.HasOne(d => d.DadoClinico)
                    .WithMany(p => p.DadosClinicoHasDoencas)
                    .HasForeignKey(d => d.DadoClinicoId)
                    .HasConstraintName("FK__DadosClin__DadoC__793DFFAF");

                entity.HasOne(d => d.Doenca)
                    .WithMany(p => p.DadosClinicoHasDoencas)
                    .HasForeignKey(d => d.DoencaId)
                    .HasConstraintName("FK__DadosClin__Doenc__7A3223E8");
            });

            modelBuilder.Entity<Doenca>(entity =>
            {
                entity.ToTable("Doenca", "Manalynx");

                entity.Property(e => e.Descricao)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Doenca1)
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasColumnName("Doenca");
            });

            modelBuilder.Entity<Equipa>(entity =>
            {
                entity.ToTable("Equipa", "Manalynx");

                entity.Property(e => e.Nome)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Regiao)
                    .HasMaxLength(127)
                    .IsUnicode(false);

                entity.HasOne(d => d.Gestor)
                    .WithMany(p => p.Equipas)
                    .HasForeignKey(d => d.GestorId)
                    .HasConstraintName("FK__Equipa__GestorId__625A9A57");
            });

            modelBuilder.Entity<Gestor>(entity =>
            {
                entity.ToTable("Gestor", "Manalynx");

                entity.HasOne(d => d.Agente)
                    .WithMany(p => p.Gestors)
                    .HasForeignKey(d => d.AgenteId)
                    .HasConstraintName("FK__Gestor__AgenteId__5F7E2DAC");
            });

            modelBuilder.Entity<Log>(entity =>
            {
                entity.ToTable("Logs", "Manalynx");

                entity.Property(e => e.LogDate)
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.Property(e => e.Query)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LoginCredential>(entity =>
            {
                entity.ToTable("LoginCredential", "Manalynx");

                entity.Property(e => e.ManaHash)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ManaSalt)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ManaUser>(entity =>
            {
                entity.ToTable("ManaUser", "Manalynx");

                entity.Property(e => e.Email)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.HasOne(d => d.LoginCredentialNavigation)
                    .WithMany(p => p.ManaUsers)
                    .HasForeignKey(d => d.LoginCredential)
                    .HasConstraintName("FK__ManaUser__LoginC__690797E6");

                entity.HasOne(d => d.Pessoa)
                    .WithMany(p => p.ManaUsers)
                    .HasForeignKey(d => d.PessoaId)
                    .HasConstraintName("FK__ManaUser__Pessoa__681373AD");
            });

            modelBuilder.Entity<Pagamento>(entity =>
            {
                entity.ToTable("Pagamento", "Manalynx");

                entity.Property(e => e.DataEmissao).HasColumnType("date");

                entity.Property(e => e.DataPagamento).HasColumnType("date");

                entity.Property(e => e.Metodo)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.HasOne(d => d.Apolice)
                    .WithMany(p => p.Pagamentos)
                    .HasForeignKey(d => d.ApoliceId)
                    .HasConstraintName("FK__Pagamento__Apoli__0C50D423");
            });

            modelBuilder.Entity<Pessoa>(entity =>
            {
                entity.ToTable("Pessoa", "Manalynx");

                entity.Property(e => e.Cc)
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasColumnName("CC");

                entity.Property(e => e.DataNascimento).HasColumnType("date");

                entity.Property(e => e.EstadoCivil).HasMaxLength(45);

                entity.Property(e => e.Nacionaldiade)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Nif).HasColumnName("NIF");

                entity.Property(e => e.Nome)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Nss).HasColumnName("NSS");

                entity.Property(e => e.Nus).HasColumnName("NUS");

                entity.Property(e => e.ValidadeCc)
                    .HasColumnType("date")
                    .HasColumnName("ValidadeCC");
            });

            modelBuilder.Entity<Seguro>(entity =>
            {
                entity.ToTable("Seguro", "Manalynx");

                entity.Property(e => e.Nome)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo).HasMaxLength(45);
            });

            modelBuilder.Entity<Sinistro>(entity =>
            {
                entity.ToTable("Sinistro", "Manalynx");

                entity.Property(e => e.DataSinistro).HasColumnType("date");

                entity.Property(e => e.Descricao)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Estado).HasMaxLength(45);

                entity.Property(e => e.RefPerito)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.RelatorioPeritagem)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Resultado)
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SinistroPessoal>(entity =>
            {
                entity.ToTable("SinistroPessoal", "Manalynx");

                entity.HasOne(d => d.ApolicePessoal)
                    .WithMany(p => p.SinistroPessoals)
                    .HasForeignKey(d => d.ApolicePessoalId)
                    .HasConstraintName("FK__SinistroP__Apoli__27F8EE98");

                entity.HasOne(d => d.Sinistro)
                    .WithMany(p => p.SinistroPessoals)
                    .HasForeignKey(d => d.SinistroId)
                    .HasConstraintName("FK__SinistroP__Sinis__28ED12D1");
            });

            modelBuilder.Entity<SinistroVeiculo>(entity =>
            {
                entity.ToTable("SinistroVeiculo", "Manalynx");

                entity.HasOne(d => d.ApoliceVeiculo)
                    .WithMany(p => p.SinistroVeiculos)
                    .HasForeignKey(d => d.ApoliceVeiculoId)
                    .HasConstraintName("FK__SinistroV__Apoli__2CBDA3B5");

                entity.HasOne(d => d.Sinistro)
                    .WithMany(p => p.SinistroVeiculos)
                    .HasForeignKey(d => d.SinistroId)
                    .HasConstraintName("FK__SinistroV__Sinis__2BC97F7C");
            });

            modelBuilder.Entity<Transacao>(entity =>
            {
                entity.ToTable("Transacao", "Manalynx");

                entity.Property(e => e.DataTransacao).HasColumnType("date");

                entity.Property(e => e.Descricao)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.HasOne(d => d.ApoliceSaude)
                    .WithMany(p => p.Transacaos)
                    .HasForeignKey(d => d.ApoliceSaudeId)
                    .HasConstraintName("FK__Transacao__Apoli__16CE6296");
            });

            modelBuilder.Entity<Tratamento>(entity =>
            {
                entity.ToTable("Tratamento", "Manalynx");

                entity.Property(e => e.Frequencia)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.NomeTratamento)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.UltimaToma).HasColumnType("date");

                entity.HasOne(d => d.DadoClinico)
                    .WithMany(p => p.Tratamentos)
                    .HasForeignKey(d => d.DadoClinicoId)
                    .HasConstraintName("FK__Tratament__DadoC__74794A92");
            });

            modelBuilder.Entity<Veiculo>(entity =>
            {
                entity.ToTable("Veiculo", "Manalynx");

                entity.Property(e => e.Marca)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Matricula)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Modelo)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Vin)
                    .HasMaxLength(17)
                    .IsUnicode(false)
                    .HasColumnName("VIN");

                entity.HasOne(d => d.CategoriaVeiculo)
                    .WithMany(p => p.Veiculos)
                    .HasForeignKey(d => d.CategoriaVeiculoId)
                    .HasConstraintName("FK__Veiculo__Categor__03BB8E22");

                entity.HasOne(d => d.Cliente)
                    .WithMany(p => p.Veiculos)
                    .HasForeignKey(d => d.ClienteId)
                    .HasConstraintName("FK__Veiculo__Cliente__04AFB25B");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
