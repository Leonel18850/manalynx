﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class RelatorioPeritagem
    {
        public int Id { get; set; }
        public string? Conteudo { get; set; }
        public DateTime DataRelatorio { get; set; }
        public bool Deferido { get; set; }
        public int? SinistroId { get; set; }

        public virtual Sinistro? Sinistro { get; set; }
    }
}
