﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class SinistroPessoal
    {
        public int Id { get; set; }
        public int? ApolicePessoalId { get; set; }
        public int? SinistroId { get; set; }

        public virtual ApolicePessoal? ApolicePessoal { get; set; }
        public virtual Sinistro? Sinistro { get; set; }
    }
}
