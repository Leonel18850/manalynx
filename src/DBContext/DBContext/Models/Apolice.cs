﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Apolice
    {
        public Apolice()
        {
            ApolicePessoals = new HashSet<ApolicePessoal>();
            ApoliceSaudes = new HashSet<ApoliceSaude>();
            ApoliceVeiculos = new HashSet<ApoliceVeiculo>();
            CoberturaHasApolices = new HashSet<CoberturaHasApolice>();
            Pagamentos = new HashSet<Pagamento>();
        }

        public int Id { get; set; }
        public bool Ativa { get; set; }
        public double? Premio { get; set; }
        public DateTime? Validade { get; set; }
        public string? Fracionamento { get; set; }
        public int? SeguroId { get; set; }
        public int? AgenteId { get; set; }

        public virtual Agente? Agente { get; set; }
        public virtual Seguro? Seguro { get; set; }
        public virtual ICollection<ApolicePessoal> ApolicePessoals { get; set; }
        public virtual ICollection<ApoliceSaude> ApoliceSaudes { get; set; }
        public virtual ICollection<ApoliceVeiculo> ApoliceVeiculos { get; set; }
        public virtual ICollection<CoberturaHasApolice> CoberturaHasApolices { get; set; }
        public virtual ICollection<Pagamento> Pagamentos { get; set; }
    }
}
