﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Tratamento
    {
        public int Id { get; set; }
        public string? NomeTratamento { get; set; }
        public string? Frequencia { get; set; }
        public DateTime? UltimaToma { get; set; }
        public int? DadoClinicoId { get; set; }

        public virtual DadoClinico? DadoClinico { get; set; }
    }
}
