﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class CoberturaHasApolice
    {
        public int Id { get; set; }
        public int? CoberturaId { get; set; }
        public int? ApoliceId { get; set; }

        public virtual Apolice? Apolice { get; set; }
        public virtual Cobertura? Cobertura { get; set; }
    }
}
