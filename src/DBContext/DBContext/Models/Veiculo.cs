﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Veiculo
    {
        public Veiculo()
        {
            ApoliceVeiculos = new HashSet<ApoliceVeiculo>();
        }

        public int Id { get; set; }
        public string Vin { get; set; } = null!;
        public string Matricula { get; set; } = null!;
        public int Ano { get; set; }
        public int Mes { get; set; }
        public string Marca { get; set; } = null!;
        public string Modelo { get; set; } = null!;
        public int? Cilindrada { get; set; }
        public int? Portas { get; set; }
        public int? Lugares { get; set; }
        public int? Potencia { get; set; }
        public int? Peso { get; set; }
        public int? CategoriaVeiculoId { get; set; }
        public int? ClienteId { get; set; }

        public virtual CategoriaVeiculo? CategoriaVeiculo { get; set; }
        public virtual Cliente? Cliente { get; set; }
        public virtual ICollection<ApoliceVeiculo> ApoliceVeiculos { get; set; }
    }
}
