﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Pessoa
    {
        public Pessoa()
        {
            Agentes = new HashSet<Agente>();
            Clientes = new HashSet<Cliente>();
            Contactos = new HashSet<Contacto>();
            ManaUsers = new HashSet<ManaUser>();
        }

        public int Id { get; set; }
        public string? Nome { get; set; }
        public DateTime? DataNascimento { get; set; }
        public string? Nacionalidade { get; set; }
        public string? Cc { get; set; }
        public DateTime? ValidadeCc { get; set; }
        public int? Nif { get; set; }
        public int? Nss { get; set; }
        public int? Nus { get; set; }
        public string? EstadoCivil { get; set; }

        public virtual ICollection<Agente> Agentes { get; set; }
        public virtual ICollection<Cliente> Clientes { get; set; }
        public virtual ICollection<Contacto> Contactos { get; set; }
        public virtual ICollection<ManaUser> ManaUsers { get; set; }
    }
}
