﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Log
    {
        public int Id { get; set; }
        public byte[]? LogDate { get; set; }
        public string? Username { get; set; }
        public string? Query { get; set; }
    }
}
