﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Seguro
    {
        public Seguro()
        {
            Apolices = new HashSet<Apolice>();
            Coberturas = new HashSet<Cobertura>();
        }

        public int Id { get; set; }
        public string Nome { get; set; } = null!;
        public bool Ativo { get; set; }
        public string Tipo { get; set; } = null!;

        public virtual ICollection<Apolice> Apolices { get; set; }
        public virtual ICollection<Cobertura> Coberturas { get; set; }
    }
}
