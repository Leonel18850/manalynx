﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class DadoClinico
    {
        public DadoClinico()
        {
            Clientes = new HashSet<Cliente>();
            DadosClinicoHasDoencas = new HashSet<DadosClinicoHasDoenca>();
            Tratamentos = new HashSet<Tratamento>();
        }

        public int Id { get; set; }
        public double? Altura { get; set; }
        public double? Peso { get; set; }
        public string? Tensao { get; set; }

        public virtual ICollection<Cliente> Clientes { get; set; }
        public virtual ICollection<DadosClinicoHasDoenca> DadosClinicoHasDoencas { get; set; }
        public virtual ICollection<Tratamento> Tratamentos { get; set; }
    }
}
