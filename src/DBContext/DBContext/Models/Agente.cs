﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Agente
    {
        public Agente()
        {
            Apolices = new HashSet<Apolice>();
            Clientes = new HashSet<Cliente>();
            Gestors = new HashSet<Gestor>();
        }

        public int Id { get; set; }
        public int Nagente { get; set; }
        public int? PessoaId { get; set; }
        public int? EquipaId { get; set; }

        public virtual Equipa? Equipa { get; set; }
        public virtual Pessoa? Pessoa { get; set; }
        public virtual ICollection<Apolice> Apolices { get; set; }
        public virtual ICollection<Cliente> Clientes { get; set; }
        public virtual ICollection<Gestor> Gestors { get; set; }
    }
}
