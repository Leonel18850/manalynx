﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Cliente
    {
        public Cliente()
        {
            ApolicePessoals = new HashSet<ApolicePessoal>();
            ApoliceSaudes = new HashSet<ApoliceSaude>();
            Veiculos = new HashSet<Veiculo>();
        }

        public int Id { get; set; }
        public string? Profissao { get; set; }
        public bool? ProfissaoRisco { get; set; }
        public int? PessoaId { get; set; }
        public int? DadoClinicoId { get; set; }
        public int? AgenteId { get; set; }

        public virtual Agente? Agente { get; set; }
        public virtual DadoClinico? DadoClinico { get; set; }
        public virtual Pessoa? Pessoa { get; set; }
        public virtual ICollection<ApolicePessoal> ApolicePessoals { get; set; }
        public virtual ICollection<ApoliceSaude> ApoliceSaudes { get; set; }
        public virtual ICollection<Veiculo> Veiculos { get; set; }
    }
}
