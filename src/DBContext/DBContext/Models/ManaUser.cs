﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class ManaUser
    {
        public int Id { get; set; }
        public string Username { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string? UserRole { get; set; }
        public int? PessoaId { get; set; }
        public int? LoginCredential { get; set; }

        public virtual LoginCredential? LoginCredentialNavigation { get; set; }
        public virtual Pessoa? Pessoa { get; set; }
    }
}
