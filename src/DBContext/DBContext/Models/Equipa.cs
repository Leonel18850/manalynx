﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Equipa
    {
        public Equipa()
        {
            Agentes = new HashSet<Agente>();
        }

        public int Id { get; set; }
        public string Nome { get; set; } = null!;
        public string Regiao { get; set; } = null!;
        public int? GestorId { get; set; }

        public virtual Gestor? Gestor { get; set; }
        public virtual ICollection<Agente> Agentes { get; set; }
    }
}
