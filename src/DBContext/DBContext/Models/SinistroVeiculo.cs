﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class SinistroVeiculo
    {
        public int Id { get; set; }
        public int? SinistroId { get; set; }
        public int? ApoliceVeiculoId { get; set; }

        public virtual ApoliceVeiculo? ApoliceVeiculo { get; set; }
        public virtual Sinistro? Sinistro { get; set; }
    }
}
