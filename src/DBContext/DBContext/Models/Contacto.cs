﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBContext.Models
{
    public partial class Contacto
    {
        [Key]
        public int Id { get; set; }
        public string Valor { get; set; } = null!;
        public string Tipo { get; set; } = null!;
        public int? PessoaId { get; set; }

        public virtual Pessoa? Pessoa { get; set; }
    }
}
