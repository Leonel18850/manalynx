﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class DadosClinicoHasDoenca
    {
        public int Id { get; set; }
        public int? DadoClinicoId { get; set; }
        public int? DoencaId { get; set; }

        public virtual DadoClinico? DadoClinico { get; set; }
        public virtual Doenca? Doenca { get; set; }
    }
}
