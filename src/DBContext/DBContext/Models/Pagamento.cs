﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Pagamento
    {
        public int Id { get; set; }
        public string Metodo { get; set; } = null!;
        public DateTime DataEmissao { get; set; }
        public DateTime DataPagamento { get; set; }
        public double Montante { get; set; }
        public int? ApoliceId { get; set; }

        public virtual Apolice? Apolice { get; set; }
    }
}
