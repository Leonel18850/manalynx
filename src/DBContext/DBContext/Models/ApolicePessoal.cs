﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class ApolicePessoal
    {
        public ApolicePessoal()
        {
            SinistroPessoals = new HashSet<SinistroPessoal>();
        }

        public int Id { get; set; }
        public int? ApoliceId { get; set; }
        public int? ClienteId { get; set; }

        public virtual Apolice? Apolice { get; set; }
        public virtual Cliente? Cliente { get; set; }
        public virtual ICollection<SinistroPessoal> SinistroPessoals { get; set; }
    }
}
