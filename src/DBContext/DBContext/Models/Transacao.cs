﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class Transacao
    {
        public int Id { get; set; }
        public string Descricao { get; set; } = null!;
        public DateTime DataTransacao { get; set; }
        public int? ApoliceSaudeId { get; set; }

        public virtual ApoliceSaude? ApoliceSaude { get; set; }
    }
}
