﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class LoginCredential
    {
        public LoginCredential()
        {
            ManaUsers = new HashSet<ManaUser>();
        }

        public int Id { get; set; }
        public string? ManaHash { get; set; }
        public string? ManaSalt { get; set; }

        public virtual ICollection<ManaUser> ManaUsers { get; set; }
    }
}
