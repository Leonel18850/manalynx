﻿using System;
using System.Collections.Generic;

namespace DBContext.Models
{
    public partial class ApoliceVeiculo
    {
        public ApoliceVeiculo()
        {
            SinistroVeiculos = new HashSet<SinistroVeiculo>();
        }

        public int Id { get; set; }
        public int? ApoliceId { get; set; }
        public int? VeiculoId { get; set; }

        public virtual Apolice? Apolice { get; set; }
        public virtual Veiculo? Veiculo { get; set; }
        public virtual ICollection<SinistroVeiculo> SinistroVeiculos { get; set; }
    }
}
