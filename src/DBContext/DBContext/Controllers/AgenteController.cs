﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBContext.Models;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Authorization;

namespace DBContext.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgenteController : Controller
    {
        /// <summary>z
        /// Controller for the Agente table from the database
        /// </summary>

        private readonly DatabaseContext _db;
        //private readonly IConfiguration _configuration;

        public AgenteController(DatabaseContext db)
        {
            _db = db;
        }

        //GETALL
        [HttpGet("index")]
        public IActionResult Index()
        {
            if (_db.Agentes != null)
            {
                var objAgenteList = _db.Agentes.ToList();
                return Ok(objAgenteList);
            }
            else return NotFound();
        }

        //GET
        [HttpGet("viewbyid")]
        public IActionResult ViewById(int? Id)
        {
            if (Id == null || Id == 0)
            {
                return NotFound();
            }
            var agenteFromDb = _db.Agentes.Find(Id);

            if (agenteFromDb == null)
            {
                return NotFound();
            }

            return View(agenteFromDb);
        }

        //POST
        [HttpPost("create")]
        public IActionResult Create(Agente obj)
        {
            if (ModelState.IsValid)
            {
                _db.Agentes.Add(obj);
                _db.SaveChanges();
                return RedirectToAction("index");
            }
            return View(obj);
        }

        //PUT
        [HttpPut("edit")]
        public IActionResult Edit(Agente obj)
        {
            if (ModelState.IsValid)
            {
                _db.Agentes.Update(obj);
                _db.SaveChanges();
                return RedirectToAction("index");
            }
            return View(obj);
        }

        //DELETE
        [HttpDelete("delete")]
        public IActionResult Delete(int? Id)
        {
            var obj = _db.Agentes.Find(Id);
            if (obj == null)
            {
                return NotFound();
            }
            _db.Agentes.Remove(obj);
            _db.SaveChanges();
            return RedirectToAction("index");
        }

    }
}
